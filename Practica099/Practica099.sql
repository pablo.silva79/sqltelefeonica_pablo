USE [master]
GO
/****** Object:  Database [bd_biodigestores]    Script Date: 14/10/2022 12:01:10 a. m. ******/
CREATE DATABASE [bd_biodigestores]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'bd_biodigestores', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\bd_biodigestores.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'bd_biodigestores_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\bd_biodigestores_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [bd_biodigestores] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [bd_biodigestores].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [bd_biodigestores] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [bd_biodigestores] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [bd_biodigestores] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [bd_biodigestores] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [bd_biodigestores] SET ARITHABORT OFF 
GO
ALTER DATABASE [bd_biodigestores] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [bd_biodigestores] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [bd_biodigestores] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [bd_biodigestores] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [bd_biodigestores] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [bd_biodigestores] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [bd_biodigestores] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [bd_biodigestores] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [bd_biodigestores] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [bd_biodigestores] SET  DISABLE_BROKER 
GO
ALTER DATABASE [bd_biodigestores] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [bd_biodigestores] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [bd_biodigestores] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [bd_biodigestores] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [bd_biodigestores] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [bd_biodigestores] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [bd_biodigestores] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [bd_biodigestores] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [bd_biodigestores] SET  MULTI_USER 
GO
ALTER DATABASE [bd_biodigestores] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [bd_biodigestores] SET DB_CHAINING OFF 
GO
ALTER DATABASE [bd_biodigestores] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [bd_biodigestores] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [bd_biodigestores] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [bd_biodigestores] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [bd_biodigestores] SET QUERY_STORE = OFF
GO
USE [bd_biodigestores]
GO
/****** Object:  Table [dbo].[biodigestores]    Script Date: 14/10/2022 12:01:10 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[biodigestores](
	[bio_id] [int] IDENTITY(1,1) NOT NULL,
	[bio_nombre] [varchar](50) NULL,
	[bio_latitud] [varchar](50) NULL,
	[bio_longitud] [varchar](50) NULL,
 CONSTRAINT [PK_BIO_ID] PRIMARY KEY CLUSTERED 
(
	[bio_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mediciones]    Script Date: 14/10/2022 12:01:10 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mediciones](
	[med_id] [int] NOT NULL,
	[sen_id] [int] NULL,
	[med_valor] [float] NULL,
	[med_fecha_hora] [datetime] NULL,
 CONSTRAINT [PK_mediciones] PRIMARY KEY CLUSTERED 
(
	[med_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[personas]    Script Date: 14/10/2022 12:01:10 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[personas](
	[per_id] [int] NOT NULL,
	[per_nombre] [varchar](50) NULL,
	[per_apellido] [varchar](50) NULL,
	[per_fecha] [date] NULL,
 CONSTRAINT [PK_personas] PRIMARY KEY CLUSTERED 
(
	[per_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[personas_biodigestores]    Script Date: 14/10/2022 12:01:10 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[personas_biodigestores](
	[per_id] [int] NULL,
	[bio_id] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sensores]    Script Date: 14/10/2022 12:01:10 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sensores](
	[sen_id] [int] NOT NULL,
	[bio_id] [int] NOT NULL,
	[tip_id] [int] NOT NULL,
	[sen_marca] [varchar](50) NULL,
	[sen_modelo] [varchar](50) NULL,
 CONSTRAINT [PK_sensores] PRIMARY KEY CLUSTERED 
(
	[sen_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tiposensor]    Script Date: 14/10/2022 12:01:10 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tiposensor](
	[tip_id] [int] NOT NULL,
	[tip_descripcion] [varchar](50) NULL,
	[tip_unidad_med] [varchar](50) NULL,
 CONSTRAINT [PK_tiposensor] PRIMARY KEY CLUSTERED 
(
	[tip_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[biodigestores] ON 

INSERT [dbo].[biodigestores] ([bio_id], [bio_nombre], [bio_latitud], [bio_longitud]) VALUES (1, N'Anakin
', N'S34°39''12.38"
', N'O58°37''11.1"
')
INSERT [dbo].[biodigestores] ([bio_id], [bio_nombre], [bio_latitud], [bio_longitud]) VALUES (2, N'Obi Wan
', N'S34°39''15.22"
', N'O58°37''13.1"
')
INSERT [dbo].[biodigestores] ([bio_id], [bio_nombre], [bio_latitud], [bio_longitud]) VALUES (3, N'Darth Vader
', N'S34°39''10.38"
', N'O58°37''09.3"
')
SET IDENTITY_INSERT [dbo].[biodigestores] OFF
GO
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (1, 1, 12.3, CAST(N'2022-10-11T08:00:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (2, 1, 12.5, CAST(N'2022-10-11T08:05:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (3, 1, 12.8, CAST(N'2022-10-11T08:10:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (4, 1, 13.1, CAST(N'2022-10-11T08:15:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (5, 1, 13.3, CAST(N'2022-10-11T08:20:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (6, 1, 12, CAST(N'2022-10-11T08:25:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (7, 2, 33.5, CAST(N'2022-10-11T08:00:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (8, 2, 43.2, CAST(N'2022-10-11T08:05:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (9, 2, 45.8, CAST(N'2022-10-11T08:10:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (10, 2, 50.2, CAST(N'2022-10-11T08:15:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (11, 2, 48.1, CAST(N'2022-10-11T08:20:00.000' AS DateTime))
INSERT [dbo].[mediciones] ([med_id], [sen_id], [med_valor], [med_fecha_hora]) VALUES (12, 2, 49.7, CAST(N'2022-10-11T08:25:00.000' AS DateTime))
GO
INSERT [dbo].[personas] ([per_id], [per_nombre], [per_apellido], [per_fecha]) VALUES (1, N'Juan', N'PEREZ', CAST(N'1985-02-13' AS Date))
INSERT [dbo].[personas] ([per_id], [per_nombre], [per_apellido], [per_fecha]) VALUES (2, N'Gabriel', N'CASAS', CAST(N'1972-11-17' AS Date))
INSERT [dbo].[personas] ([per_id], [per_nombre], [per_apellido], [per_fecha]) VALUES (3, N'Susana', N'ROMERO', CAST(N'1990-05-13' AS Date))
GO
INSERT [dbo].[personas_biodigestores] ([per_id], [bio_id]) VALUES (1, 1)
INSERT [dbo].[personas_biodigestores] ([per_id], [bio_id]) VALUES (1, 2)
INSERT [dbo].[personas_biodigestores] ([per_id], [bio_id]) VALUES (1, 3)
INSERT [dbo].[personas_biodigestores] ([per_id], [bio_id]) VALUES (2, 1)
INSERT [dbo].[personas_biodigestores] ([per_id], [bio_id]) VALUES (2, 3)
INSERT [dbo].[personas_biodigestores] ([per_id], [bio_id]) VALUES (3, 3)
GO
INSERT [dbo].[sensores] ([sen_id], [bio_id], [tip_id], [sen_marca], [sen_modelo]) VALUES (1, 1, 1, N'IST
', N'AG OUT
')
INSERT [dbo].[sensores] ([sen_id], [bio_id], [tip_id], [sen_marca], [sen_modelo]) VALUES (2, 1, 2, N'HUMIDITY
', N'H1101
')
INSERT [dbo].[sensores] ([sen_id], [bio_id], [tip_id], [sen_marca], [sen_modelo]) VALUES (3, 1, 3, N'HONEYWELL
', N'MLH
')
INSERT [dbo].[sensores] ([sen_id], [bio_id], [tip_id], [sen_marca], [sen_modelo]) VALUES (4, 2, 2, N'HUMIDITY
', N'H1101
')
INSERT [dbo].[sensores] ([sen_id], [bio_id], [tip_id], [sen_marca], [sen_modelo]) VALUES (5, 2, 3, N'HONEYWELL
', N'MLH
')
INSERT [dbo].[sensores] ([sen_id], [bio_id], [tip_id], [sen_marca], [sen_modelo]) VALUES (6, 3, 1, N'IST
', N'AG OUT')
GO
INSERT [dbo].[tiposensor] ([tip_id], [tip_descripcion], [tip_unidad_med]) VALUES (1, N'Temperatura', N'Grados Centigrados
')
INSERT [dbo].[tiposensor] ([tip_id], [tip_descripcion], [tip_unidad_med]) VALUES (2, N'Humedad', N'%
')
INSERT [dbo].[tiposensor] ([tip_id], [tip_descripcion], [tip_unidad_med]) VALUES (3, N'Presion', N'PSI
')
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [unique_bio_nombre]    Script Date: 14/10/2022 12:01:10 a. m. ******/
ALTER TABLE [dbo].[biodigestores] ADD  CONSTRAINT [unique_bio_nombre] UNIQUE NONCLUSTERED 
(
	[bio_nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[mediciones]  WITH CHECK ADD  CONSTRAINT [FK_mediciones_sensores] FOREIGN KEY([sen_id])
REFERENCES [dbo].[sensores] ([sen_id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[mediciones] CHECK CONSTRAINT [FK_mediciones_sensores]
GO
ALTER TABLE [dbo].[personas_biodigestores]  WITH CHECK ADD  CONSTRAINT [FK_personas_biodigestores_biodigestores] FOREIGN KEY([bio_id])
REFERENCES [dbo].[biodigestores] ([bio_id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[personas_biodigestores] CHECK CONSTRAINT [FK_personas_biodigestores_biodigestores]
GO
ALTER TABLE [dbo].[personas_biodigestores]  WITH CHECK ADD  CONSTRAINT [FK_personas_biodigestores_personas] FOREIGN KEY([per_id])
REFERENCES [dbo].[personas] ([per_id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[personas_biodigestores] CHECK CONSTRAINT [FK_personas_biodigestores_personas]
GO
ALTER TABLE [dbo].[sensores]  WITH CHECK ADD  CONSTRAINT [FK_sensores_biosensores] FOREIGN KEY([bio_id])
REFERENCES [dbo].[biodigestores] ([bio_id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[sensores] CHECK CONSTRAINT [FK_sensores_biosensores]
GO
ALTER TABLE [dbo].[sensores]  WITH CHECK ADD  CONSTRAINT [FK_sensores_tiposensores] FOREIGN KEY([tip_id])
REFERENCES [dbo].[tiposensor] ([tip_id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[sensores] CHECK CONSTRAINT [FK_sensores_tiposensores]
GO
USE [master]
GO
ALTER DATABASE [bd_biodigestores] SET  READ_WRITE 
GO
